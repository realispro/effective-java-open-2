package eu.sages.ej.solid.impl;

import eu.sages.ej.solid.Announcer;
import eu.sages.ej.solid.Employee;

public class Director extends Employee implements Announcer {

    public Director(String name, int grade) {
        super(name, grade);
    }

}
