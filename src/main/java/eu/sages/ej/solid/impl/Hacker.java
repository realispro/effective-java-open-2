package eu.sages.ej.solid.impl;

import eu.sages.ej.solid.Employee;
import eu.sages.ej.solid.IssueHandler;

public class Hacker extends Employee implements IssueHandler {

    public Hacker(String name, int grade) {
        super(name, grade);
    }

    public void doHacking(){
        System.out.println("doing hacking...");
    }

    @Override
    public void handleIssue(String issue, IssueHandler support){
        System.out.println("handling issue = " + issue);
    }

}
