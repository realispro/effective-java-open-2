package eu.sages.ej.solid.impl;

import eu.sages.ej.solid.Announcer;
import eu.sages.ej.solid.Employee;

import java.util.Date;
import java.util.Random;

public class Manager extends Employee implements Announcer {


    public Manager(String name, int grade) {
        super(name, grade);
    }

    // close - context
    public final void doAssessment(/* open */ Employee employee){
        int rate = new Random(new Date().getTime()).nextInt(10)+1;
        if(rate>=5){
            employee.setGrade(employee.getGrade()+1);
        }
    }

    public final void doAssessment(/* open */ Magician magician){
        magician.setGrade(magician.getGrade()+1);
    }


}
