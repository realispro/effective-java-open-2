package eu.sages.ej.solid.impl;

import eu.sages.ej.solid.IssueHandler;

public class Intern implements IssueHandler {
    @Override
    public void handleIssue(String issue, IssueHandler support) {
        System.out.println("intern is handling issue " + issue);
    }
}
