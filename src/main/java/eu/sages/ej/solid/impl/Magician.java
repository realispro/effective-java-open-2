package eu.sages.ej.solid.impl;

import eu.sages.ej.solid.Employee;

public class Magician extends Employee {

    public Magician(String name, int grade) {
        super(name, grade);
    }

    public void doMagic(){
        System.out.println("doing magic...");
    }

}
