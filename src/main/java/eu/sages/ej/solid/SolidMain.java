package eu.sages.ej.solid;

import eu.sages.ej.solid.impl.Director;
import eu.sages.ej.solid.impl.Hacker;
import eu.sages.ej.solid.impl.Magician;
import eu.sages.ej.solid.impl.Manager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SolidMain {

    public static void main(String[] args) {
        System.out.println("SolidMain.main");

        Director director = new Director("Nowak", 9);
        Manager manager = new Manager("Kowalski", 5);

        Magician magician = new Magician("Gandalf", 7);
        magician.doMagic();

        Hacker hacker = new Hacker("Mitnick", 8);
        hacker.doHacking();

        String issue = "critical error!";
        IssueHandler handler = (i, support) -> System.out.println("AI: looking for a solution of an issue " + issue);
                //new Intern();
        handler.handleIssue(issue, hacker);
        
        // assessment time
        String message = "Assessment time!";
        Announcer announcer = director;
        announcer.sendMessage(message);

        manager.doAssessment((Employee)magician);
        manager.doAssessment(hacker);

        System.out.println("magician = " + magician);

        List<String> students = new ArrayList<>(
                Arrays.asList("Joanna", "Michal", "Piotr", "Maciej", "Pawel")
        );
        students.add("Marcin");
        for(String student : students){
            System.out.println("student = " + student);
        }
    }
}
