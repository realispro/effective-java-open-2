package eu.sages.ej.solid;

public interface IssueHandler {

    void handleIssue(String issue, IssueHandler handler);
}
