package eu.sages.ej.solid;

public interface Announcer {

    default void sendMessage(String message) {
        System.out.println("message = " + message);
    }
}
