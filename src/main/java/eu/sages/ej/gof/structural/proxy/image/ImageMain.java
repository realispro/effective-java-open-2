package eu.sages.ej.gof.structural.proxy.image;

public class ImageMain {

    public static void main(String[] args) {
        System.out.println("let's display some images");

        Image i1 = new ImageProxy("image1.jpg");
        Image i2 = new ImageProxy("image2.jpg");
        Image i3 = new ImageProxy("image3.jpg");

        byte[] data = i1.getData();
        System.out.println("displaying image: " + new String(data));
    }
}
