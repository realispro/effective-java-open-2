package eu.sages.ej.gof.structural.proxy.image;

// Proxy
public class ImageProxy implements Image{

    private String fileName;

    public ImageProxy(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public byte[] getData() {
        Image img = new ImageFile(fileName);
        return img.getData();
    }
}
