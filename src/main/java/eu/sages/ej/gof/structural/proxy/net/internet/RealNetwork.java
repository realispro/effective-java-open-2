package eu.sages.ej.gof.structural.proxy.net.internet;


import eu.sages.ej.gof.structural.proxy.net.Internet;

public class RealNetwork implements Internet {

    RealNetwork(){}

    @Override
    public void connectTo(String address) {
        System.out.println("connecting to " + address);
    }
}
