package eu.sages.ej.gof.structural.adapter.sim;

import java.nio.charset.StandardCharsets;

// Adapter
public class Mini2SimAdapter implements Sim {

    private MiniSimCard mini;

    public Mini2SimAdapter(MiniSimCard mini) {
        this.mini = mini;
    }

    @Override
    public String getId() {
        System.out.println("adapting mini to sim....");
        return new String(mini.getId(), StandardCharsets.UTF_8);
    }
}
