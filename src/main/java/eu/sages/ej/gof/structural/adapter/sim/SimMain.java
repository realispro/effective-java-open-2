package eu.sages.ej.gof.structural.adapter.sim;

public class SimMain {

    public static void main(String[] args) {
        System.out.println("let's install sim card");

        Device d = new Device();
        Sim sim = new Mini2SimAdapter(new MiniSimCard("876555"));
        d.install(sim);

    }
}
