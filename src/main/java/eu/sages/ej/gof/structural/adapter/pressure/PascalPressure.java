package eu.sages.ej.gof.structural.adapter.pressure;

// Target
public interface PascalPressure {

    float getPressure();
}
