package eu.sages.ej.gof.structural.decorator.email;

// ConcreteComponent
public class EmailMessage implements Email {

    private String title;

    private String content;

    public EmailMessage(String title, String content) {
        this.title = title;
        this.content = content;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getContent() {
        return content;
    }
}
