package eu.sages.ej.gof.structural.proxy.net;

import eu.sages.ej.gof.structural.proxy.net.internet.InternetProvider;

public class InternetMain {

    public static void main(String[] args) {
        System.out.println("lets surf!");

        Internet internet = InternetProvider.provideInternet();

        internet.connectTo("wp.pl");
        internet.connectTo("facebook.com/events");

    }
}
