package eu.sages.ej.gof.structural.proxy.net.internet;

import eu.sages.ej.gof.structural.proxy.net.Internet;

public class InternetProxy implements Internet {

    private Internet internet;
    private static final String ERROR_ADDRESS = "error.pl";

    InternetProxy(Internet internet){
        this.internet = internet;
    }

    @Override
    public void connectTo(String address) {
        if(address.equals("facebook.com/events")) {
            address = ERROR_ADDRESS;
        }
        internet.connectTo(address);
    }
}
