package eu.sages.ej.gof.structural.decorator.sandwich;

public class SandwichMain {

    public static void main(String[] args) {
        System.out.println("let's eat some sandwich");

        Sandwich sandwich =
                new TomatoDecorator(
                    new CheeseDecorator(
                            new ButterDecorator(
                                    new WhiteBread())));

        System.out.println("eating " + sandwich.content());
    }
}
