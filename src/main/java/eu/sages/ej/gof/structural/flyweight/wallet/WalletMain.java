package eu.sages.ej.gof.structural.flyweight.wallet;

import java.util.ArrayList;
import java.util.List;

public class WalletMain {

    public static void main(String[] args) {
        System.out.println("let's put some coins into wallet");

        List<Coin> wallet = new ArrayList<>();
        wallet.add(CoinCache.getInstance().getCoin(5, Currency.PLN));
        wallet.add(CoinCache.getInstance().getCoin(5, Currency.PLN));
        wallet.add(CoinCache.getInstance().getCoin(5, Currency.PLN));
        wallet.add(CoinCache.getInstance().getCoin(5, Currency.PLN));
        wallet.add(CoinCache.getInstance().getCoin(5, Currency.PLN));
        wallet.add(CoinCache.getInstance().getCoin(5, Currency.PLN));
        wallet.add(CoinCache.getInstance().getCoin(2, Currency.PLN));
        wallet.add(CoinCache.getInstance().getCoin(2, Currency.EURO));
        wallet.add(CoinCache.getInstance().getCoin(2, Currency.DOLLAR));
        wallet.add(CoinCache.getInstance().getCoin(2, Currency.EURO));
        wallet.add(CoinCache.getInstance().getCoin(5, Currency.PLN));
        wallet.add(CoinCache.getInstance().getCoin(1, Currency.PLN));

        System.out.println("wallet content: " + wallet);



    }
}
