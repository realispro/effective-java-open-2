package eu.sages.ej.gof.structural.proxy.image;

// Subject
public interface Image {

    byte[] getData();

}
