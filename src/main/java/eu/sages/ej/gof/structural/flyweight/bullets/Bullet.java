package eu.sages.ej.gof.structural.flyweight.bullets;

public class Bullet {

    private final BulletCaliber bulletCaliber;

    private int coordX;

    private int coordY;

    private double vector;

    public Bullet(int coordX, int coordY, double vector, int caliber) {
        this.coordX = coordX;
        this.coordY = coordY;
        this.vector = vector;
        this.bulletCaliber = BulletCaliberCache.getInstance().getCaliber(caliber);
    }

    public int getCoordX() {
        return coordX;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public int getCoordY() {
        return coordY;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }


    public double getVector() {
        return vector;
    }

    public void setVector(double vector) {
        this.vector = vector;
    }

    public int getCaliber() {
        return bulletCaliber.getCaliber();
    }

    public String getIcon() {
        return bulletCaliber.getIcon();
    }

}
