package eu.sages.ej.gof.structural.proxy.net.internet;

import eu.sages.ej.gof.structural.proxy.net.Internet;

public class InternetProvider {

    private static Internet internet = new AuditProxy(new InternetProxy(new RealNetwork()));

    public static Internet provideInternet(){
        return internet;
    }
}
