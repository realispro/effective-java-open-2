package eu.sages.ej.gof.structural.decorator.email;

// Component
public interface Email {

    String getTitle();

    String getContent();
}
