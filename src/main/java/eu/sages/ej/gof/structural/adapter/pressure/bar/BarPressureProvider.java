package eu.sages.ej.gof.structural.adapter.pressure.bar;

import java.util.Random;

// Adaptee
public class BarPressureProvider {

    public float getPressure(){
        return new Random().nextFloat();
    }

}
