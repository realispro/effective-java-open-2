package eu.sages.ej.gof.structural.decorator.sandwich;

public class TomatoDecorator extends AbstractDecorator{

    public TomatoDecorator(Sandwich delegate) {
        super(delegate);
    }

    @Override
    public String getAddon() {
        return "tomato";
    }
}
