package eu.sages.ej.gof.structural.adapter.pressure;

import eu.sages.ej.gof.structural.adapter.pressure.bar.Bar2PascalPressureAdapter;

public class PressureMain {

    public static void main(String[] args) {
        System.out.println("let's measure a pressure !");

        PascalPressure pp = new Bar2PascalPressureAdapter();
        Display display = new Display();
        display.showPresure(pp);

    }
}
