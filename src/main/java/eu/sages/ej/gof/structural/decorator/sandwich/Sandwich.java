package eu.sages.ej.gof.structural.decorator.sandwich;

// Component
public interface Sandwich {

    String content();
}
