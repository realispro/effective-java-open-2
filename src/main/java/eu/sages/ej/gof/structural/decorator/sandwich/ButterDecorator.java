package eu.sages.ej.gof.structural.decorator.sandwich;

public class ButterDecorator implements Sandwich{

    private Sandwich delegate;

    public ButterDecorator(Sandwich delegate) {
        this.delegate = delegate;
    }

    @Override
    public String content() {
        return delegate.content() + ", butter";
    }
}
