package eu.sages.ej.gof.structural.proxy.image;

// RealSubject
public class ImageFile implements Image{

    private byte[] data;

    public ImageFile(String fileName){
        System.out.println("loading file " + fileName);
        data = fileName.getBytes(); // file loading emulation
    }

    @Override
    public byte[] getData() {
        return data;
    }
}
