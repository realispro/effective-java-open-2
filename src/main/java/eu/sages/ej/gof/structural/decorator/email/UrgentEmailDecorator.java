package eu.sages.ej.gof.structural.decorator.email;

public class UrgentEmailDecorator implements Email{

    private Email email;

    public UrgentEmailDecorator(Email email) {
        this.email = email;
    }

    @Override
    public String getTitle() {
        return email.getTitle().toUpperCase();
    }

    @Override
    public String getContent() {
        return email.getContent();
    }
}
