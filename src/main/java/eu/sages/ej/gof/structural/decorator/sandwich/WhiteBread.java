package eu.sages.ej.gof.structural.decorator.sandwich;

// ConcreteComponent
public class WhiteBread implements Sandwich{
    @Override
    public String content() {
        return "white bread";
    }
}
