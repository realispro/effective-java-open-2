package eu.sages.ej.gof.structural.decorator.sandwich;

public class CheeseDecorator extends AbstractDecorator{

    public CheeseDecorator(Sandwich delegate) {
        super(delegate);
    }

    @Override
    public String getAddon() {
        return "cheese";
    }
}
