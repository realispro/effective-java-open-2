package eu.sages.ej.gof.structural.flyweight.wallet;

// immutable - flyweight
public class Coin {

    private final int value;

    private final Currency currency;

    public Coin(int value, Currency currency) {
        System.out.println("[creating coin of value " + value + " and currency " + currency + "]");
        this.value = value;
        this.currency = currency;
    }

    public int getValue() {
        return value;
    }

    public Currency getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Coin{");
        sb.append("value=").append(value);
        sb.append(", currency=").append(currency);
        sb.append('}');
        return sb.toString();
    }
}
