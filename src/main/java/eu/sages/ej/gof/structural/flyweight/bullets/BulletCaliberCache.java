package eu.sages.ej.gof.structural.flyweight.bullets;

import java.util.HashMap;
import java.util.Map;

public class BulletCaliberCache {

    private Map<Integer, BulletCaliber> map = new HashMap<>();

    private static BulletCaliberCache instance;

    private BulletCaliberCache(){}

    public synchronized static BulletCaliberCache getInstance(){
        if(instance==null){
            instance = new BulletCaliberCache();
        }
        return instance;
    }




    public BulletCaliber getCaliber(int caliber){
        BulletCaliber bulletCaliber = map.get(caliber);
        if(bulletCaliber==null){
            bulletCaliber = new BulletCaliber(caliber);
            map.put(caliber, bulletCaliber);
        }
        return bulletCaliber;
    }

}
