package eu.sages.ej.gof.structural.adapter.pressure;

// Client
public class Display {

    public void showPresure(PascalPressure pp){
        System.out.println("current presure in Pascal: " + pp.getPressure());
    }
}
