package eu.sages.ej.gof.structural.decorator.email;

public class SignedEmail extends AbstractFooter {

    private final EmailAuthor emailAuthor;

    public SignedEmail(Email unsignedEmail, EmailAuthor emailAuthor) {
        super(unsignedEmail);
        this.emailAuthor = emailAuthor;
    }

    @Override
    public String getFooter() {
        return emailAuthor.getEmailSignature();
    }
}
