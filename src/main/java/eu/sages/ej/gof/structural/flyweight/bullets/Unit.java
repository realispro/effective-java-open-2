package eu.sages.ej.gof.structural.flyweight.bullets;

import java.util.Random;

public class Unit {

    private String id;

    private int health;

    private int caliber;

    public Unit(String id, int health, int caliber) {
        this.id = id;
        this.health = health;
        this.caliber = caliber;
    }

    void hit(Bullet p){
        health -= p.getCaliber();
        System.out.println( id + " has been hit");
        if(health<=0){
            System.out.println( id + " is dead");
        }
    }

    void fire(Unit u){
        if(health>0){
            System.out.println(id + " is firing to " + u.id);
            Random r = new Random();
            Bullet p = new Bullet(r.nextInt(1000), r.nextInt(1000), r.nextDouble(), caliber);
            // TODO verify coordinates and vector
            u.hit(p);
        }
    }

}
