package eu.sages.ej.gof.structural.decorator.email;

public class EmailMain {


    public static void main(String[] args) {
        System.out.println("let's send some email");

        Email email = new UrgentEmailDecorator(
                new SignedEmail(
                        new EmailMessage("Christmas time", "7 days to Christmas Eve :)"),
                        EmailAuthor.JOHN));

        System.out.println("sending email: \ntitle=[" + email.getTitle() + "] \ncontent=[\n" + email.getContent() + "\n]");

    }

}
