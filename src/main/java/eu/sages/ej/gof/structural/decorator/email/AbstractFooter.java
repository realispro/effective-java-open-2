package eu.sages.ej.gof.structural.decorator.email;

public abstract class AbstractFooter implements Email {

    private Email email;

    public AbstractFooter(Email email) {
        this.email = email;
    }

    public abstract String getFooter();

    @Override
    public String getContent() {
        return email.getContent() + "\n" + getFooter();
    }

    @Override
    public String getTitle() {
        return email.getTitle();
    }

}
