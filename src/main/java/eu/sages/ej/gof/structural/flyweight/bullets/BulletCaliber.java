package eu.sages.ej.gof.structural.flyweight.bullets;

// immutable - flyweight
public class BulletCaliber {

    private final int caliber;

    private final String icon;

    public BulletCaliber(int caliber) {
        System.out.println("[creating bullet caliber: " + caliber + "]");
        this.caliber = caliber;
        this.icon = caliber + ".jpg";
    }

    public int getCaliber() {
        return this.caliber;
    }

    public String getIcon() {
        return this.icon;
    }
}