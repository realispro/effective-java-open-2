package eu.sages.ej.gof.structural.flyweight.wallet;

public enum Currency {

    PLN,
    EURO,
    DOLLAR
}
