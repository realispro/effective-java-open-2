package eu.sages.ej.gof.structural.adapter.pressure.bar;

import eu.sages.ej.gof.structural.adapter.pressure.PascalPressure;

// Adapter
public class Bar2PascalPressureAdapter implements PascalPressure {

    private BarPressureProvider barPressureProvider = new BarPressureProvider();



    @Override
    public float getPressure() {
        return barPressureProvider.getPressure() * 100_000;
    }
}
