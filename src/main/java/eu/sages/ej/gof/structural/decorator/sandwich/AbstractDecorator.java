package eu.sages.ej.gof.structural.decorator.sandwich;

public abstract class AbstractDecorator implements Sandwich{

    private Sandwich delegate;

    public AbstractDecorator(Sandwich delegate) {
        this.delegate = delegate;
    }

    @Override
    public final String content() {
        return delegate.content() + ", " + getAddon();
    }

    public abstract String getAddon();
}
