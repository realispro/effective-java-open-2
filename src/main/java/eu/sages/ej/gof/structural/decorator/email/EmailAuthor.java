package eu.sages.ej.gof.structural.decorator.email;

public enum EmailAuthor {

    JOHN("Regards,\nJohn"),
    MARTIN("Viel Grüß,\nMartin Schmidt");

    private String emailSignature;

    EmailAuthor(String emailSignature) {
        this.emailSignature = emailSignature;
    }

    public String getEmailSignature() {
        return emailSignature;
    }
}
