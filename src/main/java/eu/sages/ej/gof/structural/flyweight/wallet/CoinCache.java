package eu.sages.ej.gof.structural.flyweight.wallet;

import java.util.HashMap;
import java.util.Map;

public class CoinCache {

    private Map<Currency, Map<Integer, Coin>> coinMap;


    private static CoinCache instance;

    private CoinCache() {
        coinMap = new HashMap<>();
    }

    public synchronized static CoinCache getInstance() {
        if (instance == null) {
            instance = new CoinCache();
        }
        return instance;
    }

    public Coin getCoin(int value, Currency currency) {
        if(!coinMap.containsKey(currency)) {
            Map<Integer, Coin> valueToCoinMap = new HashMap<>();
            coinMap.put(currency, valueToCoinMap);
        }
        if (!coinMap.get(currency).containsKey(value)) {
            coinMap.get(currency).put(value, new Coin(value, currency));
        }
        return coinMap.get(currency).get(value);
    }
}
