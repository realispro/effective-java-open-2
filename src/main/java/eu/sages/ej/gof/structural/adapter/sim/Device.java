package eu.sages.ej.gof.structural.adapter.sim;

// Client
public class Device {

    private String simId;

    public void install(Sim sim){
        simId = sim.getId();
        System.out.println("sim installed:" + simId);
    }

}
