package eu.sages.ej.gof.structural.proxy.net.internet;

import eu.sages.ej.gof.structural.proxy.net.Internet;

public class AuditProxy implements Internet {

    private Internet internet;

    AuditProxy(Internet internet) {
        this.internet = internet;
    }

    @Override
    public void connectTo(String address) {
        System.out.println("about to connect to: " + address);
        internet.connectTo(address);
    }
}
