package eu.sages.ej.gof.creational.singleton.sample;

public enum EnumSingleton {

    INSTANCE;

    private EnumSingleton(){
    }

    public static EnumSingleton getInstance(){
        return INSTANCE;
    }

    public void doLogic(){
        System.out.println("doing logic...");
    }

}
