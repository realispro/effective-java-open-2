package eu.sages.ej.gof.creational.factory.method.notification;

import eu.sages.ej.gof.creational.factory.method.notification.impl.PushNotificationFactory;
import eu.sages.ej.gof.creational.factory.method.notification.impl.SMSNotificationFactory;

public class NotificationMain {

    public static void main(String[] args) {
        System.out.println("NotificationMain.main");

        //NotificationFactory factory = new NotificationFactory();
        NotificationFactory factory = //NotificationFactoryProvider.getNotificationFactory();
                new PushNotificationFactory();

        NotificationContext context = new NotificationContext(factory);
        context.execute();
    }
}
