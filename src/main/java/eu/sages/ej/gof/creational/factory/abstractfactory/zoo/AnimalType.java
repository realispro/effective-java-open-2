package eu.sages.ej.gof.creational.factory.abstractfactory.zoo;

public enum AnimalType {

    PREHISTORIC,
    PREDATOR
}
