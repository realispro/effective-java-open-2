package eu.sages.ej.gof.creational.factory.abstractfactory.zoo;

public interface Mammal {

    void move();
}
