package eu.sages.ej.gof.creational.factory.method.notification.impl;

import eu.sages.ej.gof.creational.factory.method.notification.Notification;
import eu.sages.ej.gof.creational.factory.method.notification.NotificationFactory;

public class PushNotificationFactory extends NotificationFactory {
    @Override
    public Notification getNotification() {
        return new PushNotification();
    }
}
