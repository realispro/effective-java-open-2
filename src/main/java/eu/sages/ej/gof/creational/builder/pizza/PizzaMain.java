package eu.sages.ej.gof.creational.builder.pizza;

public class PizzaMain {

    public static void main(String[] args) {
        System.out.println("PizzaMain.main");

        Pizza pizza = new Waiter().order(new WiejskaBuilder().setDough("srednie").setSause("ketchup"));

       /*
        pizza.setDough("grube");
        pizza.addTopping("boczek");
        pizza.addTopping("cebula");
        pizza.addTopping("pieczarki");
        pizza.setSause("czosnkowy");
        */

        System.out.println("eating pizza = " + pizza);
    }

}
