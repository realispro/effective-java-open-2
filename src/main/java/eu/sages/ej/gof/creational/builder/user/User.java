package eu.sages.ej.gof.creational.builder.user;

import java.time.LocalDate;

public class User {

    //All final attributes
    private final String firstName; // required
    private final String lastName; // required
    private final LocalDate bornDate; // optional
    private final String phone; // optional
    private final String address; // optional
    private final boolean active;

    private User(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.bornDate = builder.bornDate;
        this.phone = builder.phone;
        this.address = builder.address;
        this.active = builder.active;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public LocalDate getBornDate() {
        return bornDate;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", bornDate=").append(bornDate);
        sb.append(", phone='").append(phone).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append(", active=").append(active);
        sb.append('}');
        return sb.toString();
    }

    // Fluent builder
    public static class Builder {

        private final String firstName;
        private final String lastName;
        private LocalDate bornDate;
        private String phone;
        private String address;
        private boolean active;

        public Builder(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public Builder bornAt(LocalDate date) {
            this.bornDate = date;
            return this;
        }

        public Builder withPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder withAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder active(){
            this.active = true;
            return this;
        }

        //Return the finally consrcuted User object
        public User build() {
            User user = new User(this);
            validateUserObject(user);
            return user;
        }

        private void validateUserObject(User user) {
            //Do some basic validations to check
            //if user object does not break any assumption of system
        }
    }
}