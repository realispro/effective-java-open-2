package eu.sages.ej.gof.creational.factory.method.notification.impl;

import eu.sages.ej.gof.creational.factory.method.notification.Notification;

public class SMSNotification extends Notification {

    @Override
    public void emit(String msg) {
        // TODO sms specific notification emission
        super.emit(msg);
    }
}
