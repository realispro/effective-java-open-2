package eu.sages.ej.gof.creational.factory.method;

public class ZywiecIPA implements Beer{


    @Override
    public float getVoltage() {
        return 3.5F;
    }

    @Override
    public String getName() {
        return "Zywiec IPA";
    }
}
