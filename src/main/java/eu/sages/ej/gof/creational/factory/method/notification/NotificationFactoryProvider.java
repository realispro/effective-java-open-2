package eu.sages.ej.gof.creational.factory.method.notification;

import eu.sages.ej.gof.creational.factory.method.notification.impl.SMSNotificationFactory;

public class NotificationFactoryProvider {


    public static NotificationFactory getNotificationFactory(){
        return new SMSNotificationFactory();
    }
}
