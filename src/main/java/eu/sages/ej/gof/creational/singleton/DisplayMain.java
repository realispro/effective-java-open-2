package eu.sages.ej.gof.creational.singleton;

public class DisplayMain {

    public static void main(String[] args) {
        System.out.println("DisplayMain.main");

        Display display1 = Display.getInstance();
        Display display2 = Display.getInstance();

        display1.show("text sent to display 1");
        display2.show("text sent to display 2");
        System.out.println(display1==display2);

        EnumDisplay eDisplay1 = EnumDisplay.getInstance();
        EnumDisplay eDisplay2 = EnumDisplay.getInstance();

        eDisplay1.show("text sent to enum display 1");
        eDisplay2.show("text sent to enum display 2");
        System.out.println(eDisplay1==eDisplay2);

    }
}
