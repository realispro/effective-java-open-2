package eu.sages.ej.gof.creational.factory.method.notification;

public abstract class Notification {


    public void emit(String msg){
        System.out.println( getClass().getSimpleName() +  ": emitting message: " + msg);
    }

}
