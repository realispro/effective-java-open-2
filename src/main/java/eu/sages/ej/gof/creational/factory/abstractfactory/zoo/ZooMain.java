package eu.sages.ej.gof.creational.factory.abstractfactory.zoo;

import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.predator.PredatorAnimalFactory;
import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.prehistoric.PrehistoricAnimalFactory;

public class ZooMain {


    public static void main(String[] args) {
        System.out.println("ZooMain.main");

        AnimalFactory factory = AnimalFactoryProvider.provideAnimalFactory(AnimalType.PREDATOR);
                //new PrehistoricAnimalFactory();
        visit(factory);
    }

    public static void visit(AnimalFactory factory){

        Fish fish = factory.provideFish();
        System.out.println("observing swimming " + fish.getClass().getSimpleName());
        fish.swim();

        Bird bird = factory.provideBird();
        System.out.println("observing flying " + bird.getClass().getSimpleName());
        bird.fly();

        Mammal mammal = factory.provideMammal();
        System.out.println("observing moving " + mammal.getClass().getSimpleName());
        mammal.move();
    }
}
