package eu.sages.ej.gof.creational.builder.pizza;

public class MargerithaBuilder extends PizzaBuilder{

    @Override
    public Pizza bake() {
        this.setToppings("cheese");
        return super.bake();
    }
}
