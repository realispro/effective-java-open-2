package eu.sages.ej.gof.creational.factory.method;

public class Radler implements Beer{

    @Override
    public float getVoltage() {
        return 1.0F;
    }

    @Override
    public String getName() {
        return "Radler";
    }
}
