package eu.sages.ej.gof.creational.factory.method;

public class ConcreteBeerFactory {

    public Beer getBeer(){
        return getBeer(BeerType.PILS);
    }

    public Beer getBeer(BeerType type){
        switch (type){
            case IPA:
                return new ZywiecIPA();
            case FRUITY:
                return new Radler();
            default:
                return new Pilsner();
        }
    }
}
