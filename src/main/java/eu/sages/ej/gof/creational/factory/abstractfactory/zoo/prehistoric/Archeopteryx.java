package eu.sages.ej.gof.creational.factory.abstractfactory.zoo.prehistoric;

import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.Bird;

public class Archeopteryx implements Bird {
    @Override
    public void fly() {
        System.out.println("archeopteryx is the first flying bird");
    }
}
