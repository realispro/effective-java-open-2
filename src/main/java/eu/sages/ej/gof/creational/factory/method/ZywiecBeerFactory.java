package eu.sages.ej.gof.creational.factory.method;

public class ZywiecBeerFactory extends BeerFactory {
    @Override
    public Beer createBeer() {
        return new ZywiecIPA();
    }
}
