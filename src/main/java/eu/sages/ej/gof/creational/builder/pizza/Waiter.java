package eu.sages.ej.gof.creational.builder.pizza;

// Director
public class Waiter {

    public Pizza order(PizzaBuilder builder){
        return builder.bake();
    }
}
