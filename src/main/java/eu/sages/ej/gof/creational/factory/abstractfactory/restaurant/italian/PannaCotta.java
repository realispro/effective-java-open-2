package eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.italian;

import eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.Dessert;

public class PannaCotta implements Dessert {
    @Override
    public void enjoy() {
        System.out.println("enjoying panna cotta");
    }
}
