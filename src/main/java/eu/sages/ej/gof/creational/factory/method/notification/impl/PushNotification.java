package eu.sages.ej.gof.creational.factory.method.notification.impl;

import eu.sages.ej.gof.creational.factory.method.notification.Notification;

public class PushNotification extends Notification {

    @Override
    public void emit(String msg) {
        // TODO push specific emission
        super.emit(msg);
    }
}
