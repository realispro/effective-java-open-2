package eu.sages.ej.gof.creational.factory.method;

public class LomzaBeerFactory extends BeerFactory {
    @Override
    public Beer createBeer() {
        return new LomzaBeer();
    }
}
