package eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.italian;

import eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.MainDish;

public class Pasta implements MainDish {
    @Override
    public void eat() {
        System.out.println("enjoying pasta");
    }
}
