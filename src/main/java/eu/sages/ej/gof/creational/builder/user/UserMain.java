package eu.sages.ej.gof.creational.builder.user;

import java.time.LocalDate;

public class UserMain {

    public static void main(String[] args) {
        User user = new User.Builder("Jack", "Reacher")
                .bornAt(LocalDate.of(2001, 1, 1))
                .withPhone("5655")
                .withAddress("Warszawa 00-950, ul. Woronicza 17")
                .active()
                .build();
        System.out.println("user = " + user);

    }
}
