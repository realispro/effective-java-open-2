package eu.sages.ej.gof.creational.factory.method;

public class LomzaBeer implements Beer {
    @Override
    public float getVoltage() {
        return 3.0F;
    }
    @Override
    public String getName() {
        return "Lomza";
    }
}
