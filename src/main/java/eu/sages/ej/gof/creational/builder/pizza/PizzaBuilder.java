package eu.sages.ej.gof.creational.builder.pizza;

import java.util.ArrayList;
import java.util.List;

public abstract class PizzaBuilder {

    private String dough;

    private List<String> toppings = new ArrayList<>();

    private String sause;


    public PizzaBuilder setDough(String dough){
        this.dough = dough;
        return this;
    }

    protected PizzaBuilder setToppings(String... toppings){
        if(this.dough==null){
            throw new IllegalArgumentException("define dough first");
        }
        for(String topping : toppings){
            this.toppings.add(topping);
        }
        return this;
    }

    public PizzaBuilder setSause(String sause){
        this.sause = sause;
        return this;
    }

    public Pizza bake(){
        if(this.dough==null){
            throw new IllegalStateException("define dough");
        }
        Pizza pizza = new Pizza();
        pizza.setDough(dough);
        pizza.setToppings(toppings);
        pizza.setSause(sause);
        return pizza;
    }

}
