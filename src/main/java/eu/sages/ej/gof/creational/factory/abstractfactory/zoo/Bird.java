package eu.sages.ej.gof.creational.factory.abstractfactory.zoo;

public interface Bird {

    void fly();
}
