package eu.sages.ej.gof.creational.builder.pizza;

import java.util.ArrayList;
import java.util.List;

public class Pizza {

    private String dough;

    private List<String> toppings = new ArrayList<>();

    private String sause;

    public String getDough() {
        return dough;
    }

    public void setDough(String dough) {
        this.dough = dough;
    }

    public List<String> getToppings() {
        return toppings;
    }

    public void addTopping(String topping) {
        toppings.add(topping);
    }

    public String getSause() {
        return sause;
    }

    public void setSause(String sause) {
        this.sause = sause;
    }

    public void setToppings(List<String> toppings) {
        this.toppings = toppings;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Pizza{");
        sb.append("dough='").append(dough).append('\'');
        sb.append(", toppings=").append(toppings);
        sb.append(", sause='").append(sause).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
