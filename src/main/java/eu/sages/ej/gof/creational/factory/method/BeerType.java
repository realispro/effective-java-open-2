package eu.sages.ej.gof.creational.factory.method;

public enum BeerType {
    PILS,
    IPA,
    FRUITY
}
