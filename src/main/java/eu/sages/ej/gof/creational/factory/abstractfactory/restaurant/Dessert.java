package eu.sages.ej.gof.creational.factory.abstractfactory.restaurant;

public interface Dessert {

    void enjoy();
}
