package eu.sages.ej.gof.creational.factory.method;

public class AnonymousBeerFactory {

    // or even static method but can have some impact for testing
    public Beer createBeer(BeerType type){
        switch (type) {
            case PILS:
                return createPils();
            case IPA:
                return createIpa();
            case FRUITY:
                return createRadler();
            default:
                throw new IllegalArgumentException("Beer with typ "+ type + " not found");
        }
    }

    public Beer createPils(){
        return new Beer() {
            @Override
            public float getVoltage() {
                return 5.2F;
            }
            @Override
            public String getName() {
                return "Pilsner";
            }
        };
    }
    public Beer createIpa(){
        return new Beer() {
            @Override
            public float getVoltage() {
                return 3.5F;
            }
            @Override
            public String getName() {
                return "Zywiec IPA";
            }
        };
    }
    public Beer createRadler(){
        return new Beer() {
            @Override
            public float getVoltage() {
                return 1.0F;
            }
            @Override
            public String getName() {
                return "Radler";
            }
        };
    }
}
