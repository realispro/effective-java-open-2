package eu.sages.ej.gof.creational.factory.abstractfactory.zoo.prehistoric;

import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.Mammal;

public class Mammut implements Mammal {
    @Override
    public void move() {
        System.out.println("Mammut is walking slowly");
    }
}
