package eu.sages.ej.gof.creational.factory.abstractfactory.restaurant;

public interface Restaurant {

    MainDish orderMainDish();

    Dessert orderDessert();

}
