package eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.mcdonald;

import eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.Dessert;

public class Flurry implements Dessert {
    @Override
    public void enjoy() {
        System.out.println("enjoying flurry");
    }
}
