package eu.sages.ej.gof.creational.factory.method.notification;

public class NotificationContext {

    private NotificationFactory factory;

    public NotificationContext(NotificationFactory factory) {
        this.factory = factory;
    }

    public void execute(){
        Notification n = factory.getNotification();
        n.emit("2021 is comming!");
    }
}
