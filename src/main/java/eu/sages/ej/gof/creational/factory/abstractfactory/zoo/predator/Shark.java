package eu.sages.ej.gof.creational.factory.abstractfactory.zoo.predator;

import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.Fish;

public class Shark implements Fish {
    @Override
    public void swim() {
        System.out.println("shark is swimming like a rocket!");
    }
}
