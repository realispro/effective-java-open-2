package eu.sages.ej.gof.creational.factory.abstractfactory.zoo.prehistoric;

import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.AnimalFactory;
import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.Bird;
import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.Fish;
import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.Mammal;

public class PrehistoricAnimalFactory implements AnimalFactory {
    @Override
    public Bird provideBird() {
        return new Archeopteryx();
    }

    @Override
    public Fish provideFish() {
        return new Ichtiozaur();
    }

    @Override
    public Mammal provideMammal() {
        return new Mammut();
    }
}
