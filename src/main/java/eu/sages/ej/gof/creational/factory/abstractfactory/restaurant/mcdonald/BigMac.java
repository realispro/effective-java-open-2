package eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.mcdonald;

import eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.MainDish;

public class BigMac implements MainDish {
    @Override
    public void eat() {
        System.out.println("consuming BigMac");
    }
}
