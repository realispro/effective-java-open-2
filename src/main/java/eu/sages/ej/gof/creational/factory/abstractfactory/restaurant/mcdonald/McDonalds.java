package eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.mcdonald;

import eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.Dessert;
import eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.MainDish;
import eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.Restaurant;

public class McDonalds implements Restaurant {

    @Override
    public MainDish orderMainDish() {
        return new BigMac();
    }

    @Override
    public Dessert orderDessert() {
        return new Flurry();
    }
}
