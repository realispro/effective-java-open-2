package eu.sages.ej.gof.creational.singleton.sample;

public class EagerSingleton {

    private static EagerSingleton instance;

    static {
        try {
            instance = new EagerSingleton();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private EagerSingleton() throws Exception{}

    public static EagerSingleton getInstance(){
        return instance;
    }

    public void doLogic(){
        System.out.println("doing logic...");
    }

}
