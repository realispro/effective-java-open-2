package eu.sages.ej.gof.creational.factory.method;

public class Pilsner implements Beer{
    @Override
    public float getVoltage() {
        return 5.2F;
    }

    @Override
    public String getName() {
        return "Pilsner";
    }
}
