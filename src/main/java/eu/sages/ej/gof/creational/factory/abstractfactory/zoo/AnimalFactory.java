package eu.sages.ej.gof.creational.factory.abstractfactory.zoo;

public interface AnimalFactory {

    Bird provideBird();

    Fish provideFish();

    Mammal provideMammal();
}
