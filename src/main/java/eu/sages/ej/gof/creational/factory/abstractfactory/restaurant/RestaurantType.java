package eu.sages.ej.gof.creational.factory.abstractfactory.restaurant;

public enum RestaurantType {

    MCDONALDS,
    ITALIAN
}
