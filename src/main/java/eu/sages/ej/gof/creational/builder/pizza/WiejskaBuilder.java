package eu.sages.ej.gof.creational.builder.pizza;

public class WiejskaBuilder extends PizzaBuilder{

    @Override
    public Pizza bake() {
        this.setToppings("boczek", "cebula", "kukurydza");
        return super.bake();
    }

}
