package eu.sages.ej.gof.creational.factory.abstractfactory.restaurant;

public class RestaurantMain {

    public static void main(String[] args) {

        System.out.println("let's go outside to eat something");

        RestaurantProvider provider = new RestaurantProvider();
        goOutForLunch(provider.goToParticularRestaurantType(RestaurantType.MCDONALDS));

    }

    private static void goOutForLunch(Restaurant restaurant){

        MainDish mainDish = restaurant.orderMainDish();
        mainDish.eat();

        Dessert dessert = restaurant.orderDessert();
        dessert.enjoy();
    }

}
