package eu.sages.ej.gof.creational.factory.abstractfactory.zoo.predator;

import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.Bird;

public class Eagle implements Bird {
    @Override
    public void fly() {
        System.out.println("eagle is flying very high");
    }
}
