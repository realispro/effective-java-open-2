package eu.sages.ej.gof.creational.factory.abstractfactory.zoo.predator;

import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.AnimalFactory;
import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.Bird;
import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.Fish;
import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.Mammal;

public class PredatorAnimalFactory implements AnimalFactory {
    @Override
    public Bird provideBird() {
        return new Eagle();
    }

    @Override
    public Fish provideFish() {
        return new Shark();
    }

    @Override
    public Mammal provideMammal() {
        return new Tiger();
    }
}
