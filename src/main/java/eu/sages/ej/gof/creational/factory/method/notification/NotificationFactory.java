package eu.sages.ej.gof.creational.factory.method.notification;

import eu.sages.ej.gof.creational.factory.method.notification.impl.PushNotification;
import eu.sages.ej.gof.creational.factory.method.notification.impl.SMSNotification;

public /* abstract */ class NotificationFactory {

    public Notification getNotification(){
        return new SMSNotification();
    }

    public Notification getNotification(NotificationType type){
        switch (type){
            case SMS:
                return new SMSNotification();
            default:
                return new PushNotification();
        }
    }

}
