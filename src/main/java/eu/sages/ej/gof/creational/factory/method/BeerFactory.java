package eu.sages.ej.gof.creational.factory.method;

public abstract class BeerFactory {

    public abstract Beer createBeer();

}
