package eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.italian;

import eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.Dessert;
import eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.MainDish;
import eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.Restaurant;

public class Marco implements Restaurant {

    @Override
    public MainDish orderMainDish() {
        return new Pasta();
    }
    @Override
    public Dessert orderDessert() {
        return new PannaCotta();
    }
}
