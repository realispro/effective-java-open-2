package eu.sages.ej.gof.creational.factory.method.notification;

public enum NotificationType {

    SMS,
    PUSH;
}
