package eu.sages.ej.gof.creational.factory.abstractfactory.zoo.prehistoric;

import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.Fish;

public class Ichtiozaur implements Fish {
    @Override
    public void swim() {
        System.out.println("ichtiozaur is swimming (but is not a fish!)");
    }
}
