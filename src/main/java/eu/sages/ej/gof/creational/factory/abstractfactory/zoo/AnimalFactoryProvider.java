package eu.sages.ej.gof.creational.factory.abstractfactory.zoo;

import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.predator.PredatorAnimalFactory;
import eu.sages.ej.gof.creational.factory.abstractfactory.zoo.prehistoric.PrehistoricAnimalFactory;

import java.util.HashMap;
import java.util.Map;

public class AnimalFactoryProvider {

    private static Map<AnimalType, AnimalFactory> factories;

    static {
        factories = new HashMap<>();
        factories.put(AnimalType.PREDATOR, new PredatorAnimalFactory());
        factories.put(AnimalType.PREHISTORIC, new PrehistoricAnimalFactory());
    }

    public static AnimalFactory provideAnimalFactory(AnimalType type){
        AnimalFactory factory = factories.get(type);
        if(factory==null){
            factory = new PrehistoricAnimalFactory();
        }
        return factory;
    }
}
