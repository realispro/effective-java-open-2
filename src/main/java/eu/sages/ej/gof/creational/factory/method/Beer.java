package eu.sages.ej.gof.creational.factory.method;

public interface Beer {

    float getVoltage();

    String getName();

}
