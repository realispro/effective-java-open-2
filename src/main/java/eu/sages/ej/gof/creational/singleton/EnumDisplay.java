package eu.sages.ej.gof.creational.singleton;

public enum EnumDisplay {

    INSTANCE;

    public static EnumDisplay getInstance(){
        return INSTANCE;
    }

    public void show(String text){
        System.out.println("[" + text + "]");
    }
}
