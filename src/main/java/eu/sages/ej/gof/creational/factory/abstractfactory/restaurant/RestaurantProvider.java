package eu.sages.ej.gof.creational.factory.abstractfactory.restaurant;

import eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.italian.Marco;
import eu.sages.ej.gof.creational.factory.abstractfactory.restaurant.mcdonald.McDonalds;

public class RestaurantProvider {

    public Restaurant goSomewhere() {
        return new McDonalds();
    }

    public Restaurant goToParticularRestaurantType(RestaurantType preferredType) {
        switch (preferredType) {
            case ITALIAN:
                return new Marco();
            case MCDONALDS:
                return new McDonalds();
            default:
                return goSomewhere();
                // TODO consider what happens when default case
        }
    }
}
