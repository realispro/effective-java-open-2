package eu.sages.ej.gof.creational.singleton.sample;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class SingletonMain {

    public static void main(String[] args) {
        System.out.println("SingletonMain.main");


        EnumSingleton singleton = EnumSingleton.getInstance();
        EnumSingleton singleton2 = null;

        try {
            Constructor constructor = EnumSingleton.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            singleton2 = (EnumSingleton) constructor.newInstance();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


        singleton.doLogic();
        singleton2.doLogic();
        System.out.println(singleton==singleton2);

    }

}
