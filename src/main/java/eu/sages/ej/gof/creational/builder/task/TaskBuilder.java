package eu.sages.ej.gof.creational.builder.task;

public class TaskBuilder {

    private int id;
    private String title;
    private String description;
    private int priority = 2;
    private boolean closed = false;

    public TaskBuilder withId(int id){
        this.id=id;
        return this;
    }

    public TaskBuilder withTitle(String title){
        this.title = title;
        return this;
    }

    public TaskBuilder withDescription(String description){
        this.description = description;
        return this;
    }

    public TaskBuilder withPriority(int priority){
        this.priority = priority;
        return this;
    }

    public TaskBuilder andOpen(){
        this.closed = false;
        return this;
    }

    public TaskBuilder andClose(){
        this.closed = true;
        return this;
    }

    public Task build(){
        Task task = new Task();
        task.setId(this.id);
        task.setDescription(this.description);
        task.setTitle(this.title);
        task.setPriority(this.priority);
        task.setClosed(this.closed);
        return task;
    }
}
