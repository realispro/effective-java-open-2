package eu.sages.ej.gof.creational.builder.task;

public class TaskMain {

    public static void main(String[] args) {
        System.out.println("TaskMain.main");

        Task task = new TaskBuilder()
                .withId(2)
                .withTitle("101 Add new feature")
                .withDescription("foo")
                .withPriority(1)
                .andOpen()
                .build();
        
        Task taskLikeJAXRSResponse = Task
                .withIdAndTitle(2,"101 Add new feature")
                .withDescription("foo")
                .withPriority(1)
                .andOpen()
                .build();

        System.out.println("task = " + task);
        System.out.println("taskLikeJAXRSResponse = " + taskLikeJAXRSResponse);
    }
}
