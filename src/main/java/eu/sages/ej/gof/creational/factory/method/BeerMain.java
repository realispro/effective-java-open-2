package eu.sages.ej.gof.creational.factory.method;

import java.util.LinkedList;
import java.util.List;

public class BeerMain {

    public static void main(String[] args) {
        System.out.println("BeerMain.main");


        List<BeerFactory> beerFactories = new LinkedList<>();
        beerFactories.add(new LomzaBeerFactory());
        beerFactories.add(new ZywiecBeerFactory());
        beerFactories.stream().map(bf -> bf.createBeer())
                .forEach(b -> System.out.println(b.getName() + ": " + b.getVoltage()));
    }
}
