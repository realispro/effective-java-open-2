package eu.sages.ej.gof.behavioral.observer.followers;

import java.util.ArrayList;
import java.util.List;

// subject + concrete observer
public class Profile implements Follower{

    private String name;

    private List<Follower> followers = new ArrayList<>();

    public Profile(String name) {
        this.name = name;
    }

    public void publish(String message) {
        Post p = new Post(name, message);
        System.out.println("[" + name + "] post generated: " + p.getMessage());
        followers.forEach(f->f.notify(p));
    }

    public void follow(Follower follower){
        followers.add(follower);
    }


    @Override
    public void notify(Post post) {
        System.out.println("[" + name + "] received post: " + post.getMessage());
    }
}
