package eu.sages.ej.gof.behavioral.observer.press.observers;

import eu.sages.ej.gof.behavioral.observer.press.News;
import eu.sages.ej.gof.behavioral.observer.press.NewsObserver;

public class SMSNews implements NewsObserver {

    private final int phoneNumber;

    public SMSNews(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    @Override
    public void consumeNews(News news) {
        System.out.println("Mobile " + phoneNumber+ " gets news: " + news);
    }
}
