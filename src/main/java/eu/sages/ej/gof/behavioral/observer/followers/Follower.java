package eu.sages.ej.gof.behavioral.observer.followers;

// abstract observer aka listener
public interface Follower {

    void notify(Post post);

}
