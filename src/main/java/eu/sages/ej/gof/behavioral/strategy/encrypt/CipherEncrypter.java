package eu.sages.ej.gof.behavioral.strategy.encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public class CipherEncrypter implements Encrypter{

    private static final String ALGORITHM = "AES";

    private static final byte[] KEY_VALUE = {
            1,2,3,4,5,6,7,8,
            1,2,3,4,5,6,7,8
    };

    @Override
    public byte[] encrypt(String s) throws Exception {
        Key key = new SecretKeySpec(KEY_VALUE, ALGORITHM);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(s.getBytes());
    }
}
