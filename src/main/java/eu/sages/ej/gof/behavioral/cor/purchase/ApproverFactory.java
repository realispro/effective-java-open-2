package eu.sages.ej.gof.behavioral.cor.purchase;

public class ApproverFactory {

    public static Approver getApproverChain() {
        Approver first = new Secretary("Ania", 1_000);

        first.nextApprover(new Director("Kamil", 10_000))
                .nextApprover(new President("BIG BOSS", 1_000_000));
        return first;
    }
}
