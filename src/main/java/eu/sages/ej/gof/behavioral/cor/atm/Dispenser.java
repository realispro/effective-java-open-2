package eu.sages.ej.gof.behavioral.cor.atm;

// Handler aka processor
public class Dispenser {

    private Dispenser dispenser;

    private int value;

    private int count;

    public Dispenser(int value, int count) {
        this.value = value;
        this.count = count;
    }


    public void dispense(Request request){

        int notesCount = request.getAmount() / value;
        int rest = request.getAmount() % value;
        if(notesCount>count){
            int diff = notesCount - count;
            notesCount = count;
            rest += diff*value;
        }
        count -= notesCount;
        System.out.println("dispensing " + notesCount + " notes of value " + value + "$");

        if(rest>0){
            request.setAmount(rest);
            if(dispenser!=null){
                dispenser.dispense(request);
            } else {
                throw new IllegalStateException("cannot fully p[rocess the request. rest: " + rest);
            }
        }


    }




    public void setNext(Dispenser dispenser) {
        this.dispenser = dispenser;
    }
}
