package eu.sages.ej.gof.behavioral.observer.press;

// abstract observer
public interface NewsObserver {

    void consumeNews(News news);

}
