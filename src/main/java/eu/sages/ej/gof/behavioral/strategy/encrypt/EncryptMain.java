package eu.sages.ej.gof.behavioral.strategy.encrypt;


public class EncryptMain {

    public static void main(String[] args){
        SafeStore store = new SafeStore();
        Encrypter encrypter = new CipherEncrypter();
        boolean stored = store.store("hide it!", encrypter);
        System.out.println("safely stored? " + stored);
    }

}
