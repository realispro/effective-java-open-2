package eu.sages.ej.gof.behavioral.observer.press;

import eu.sages.ej.gof.behavioral.observer.press.observers.NewsWebsite;
import eu.sages.ej.gof.behavioral.observer.press.observers.SMSNews;

public class NewsMain {

    public static void main(String[] args) {

        PressOffice po = new PressOffice();

        po.addObserver(new NewsWebsite("facebook"));
        po.addObserver(new NewsWebsite("twitter"));
        po.addObserver(new SMSNews(123456));

        po.dayWork();

    }
}
