package eu.sages.ej.gof.behavioral.strategy.encrypt;


import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class SafeStore {

    public final boolean store(Object o, Encrypter encrypter){
        try {
            byte[] toStore = encrypter.encrypt(o.toString());
            storeEncrypted(toStore);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void storeEncrypted(byte[] bytes){

        // try-with-resources
        try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("target/safe.store"))){
            bos.write(bytes);
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }
}
