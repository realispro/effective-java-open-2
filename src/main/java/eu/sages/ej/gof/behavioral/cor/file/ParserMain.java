package eu.sages.ej.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;

public class ParserMain {

    public static void main(String[] args) throws IOException {
        System.out.println("let's parse a file");

        File file = new File("./src/main/resources/cor/cor.doc");
        Parser parser = getChain();
        parser.parse(file);
    }

    private static Parser getChain(){
        TxtParser txtParser = new TxtParser();
        CsvParser csvParser = new CsvParser();
        txtParser.setNext(csvParser);
        return txtParser;
    }



}
