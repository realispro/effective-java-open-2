package eu.sages.ej.gof.behavioral.observer.press;


import java.util.HashSet;
import java.util.Set;

// subject
public class PressOffice {

    private final Set<NewsObserver> observers = new HashSet<>();

    public void addObserver(NewsObserver newsObserver){
        observers.add(newsObserver);
    }

    public void dayWork(){

        broadcast(new News("Mundial: Poland won!", "Polska mistrzem Polski"));
        broadcast(new News("Chemitrails are fake", "Chemitrails doesn't exists"));
    }

    private void broadcast(News news){
        observers.forEach(o->o.consumeNews(news));
    }



}
