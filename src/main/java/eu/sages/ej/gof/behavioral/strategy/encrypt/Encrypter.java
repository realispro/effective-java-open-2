package eu.sages.ej.gof.behavioral.strategy.encrypt;

// strategy
public interface Encrypter {

    byte[] encrypt(String string) throws Exception;
}
