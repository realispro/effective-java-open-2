package eu.sages.ej.gof.behavioral.mediator.chat;

import java.util.ArrayList;
import java.util.List;

public class Channel {

    private List<ChatPart> parts = new ArrayList<>();

    private String name;

    public Channel(String name) {
        this.name = name;
    }

    public void join(ChatPart part){
        parts.add(part);
    }

    public void send(ChatPart author, String message){
        parts.stream().filter(p->p!=author).forEach(p-> p.receive("*" + author.getName() + "* " + message));
    }
}
