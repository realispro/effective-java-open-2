package eu.sages.ej.gof.behavioral.templatemethod.encrypt;


import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public abstract class SafeStore {

    // primitive operation
    protected abstract byte[] encrypt(String s) throws Exception;

    // template method
    public final boolean store(Object o){
        try {
            byte[] toStore = encrypt(o.toString());
            storeEncrypted(toStore);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void storeEncrypted(byte[] bytes){

        // try-with-resources
        try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("target/safe.store"))){
            bos.write(bytes);
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }
}
