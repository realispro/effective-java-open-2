package eu.sages.ej.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;

public abstract class Parser {

    private Parser next;

    // template method
    public final void parse(File file) throws IOException{

        if(file.getName().endsWith(getSupportedExtension())) {
            doParsing(file);
        } else if(next!=null) {
            next.parse(file);
        }
    }

    // primary operation 1
    protected abstract String getSupportedExtension();

    // primary operation 2
    protected abstract void doParsing(File file) throws IOException;

    public void setNext(Parser parser){
        this.next = parser;
    }
}
