package eu.sages.ej.gof.behavioral.templatemethod.encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public abstract class CipherStore extends SafeStore{

    private static final byte[] keyValue = {
            1,2,3,4,5,6,7,8,
            1,2,3,4,5,6,7,8
    };

    protected abstract String getAlgorithm();

    @Override
    protected byte[] encrypt(String s) throws Exception {
        Key key = new SecretKeySpec(keyValue, getAlgorithm());
        Cipher cipher = Cipher.getInstance(getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(s.getBytes());
    }
}
