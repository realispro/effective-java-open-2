package eu.sages.ej.gof.behavioral.observer.followers;

public class Post {

    private String message;

    private String profileName;

    public Post(String profileName, String message) {
        this.profileName = profileName;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
