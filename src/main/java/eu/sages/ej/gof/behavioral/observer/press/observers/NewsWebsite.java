package eu.sages.ej.gof.behavioral.observer.press.observers;

import eu.sages.ej.gof.behavioral.observer.press.News;
import eu.sages.ej.gof.behavioral.observer.press.NewsObserver;

// concrete observer
public class NewsWebsite implements NewsObserver {

    private final String websiteName;

    public NewsWebsite(String websiteName) {
        this.websiteName = websiteName;
    }

    @Override
    public void consumeNews(News news) {
        System.out.println(websiteName+ " gets news: " + news);
    }
}