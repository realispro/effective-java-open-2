package eu.sages.ej.gof.behavioral.cor.atm;

public class ATM {

    private Dispenser dispenser = getChain();

    public ATM() {
        // TODO construct dispenser chain
    }

    public void withdraw(int amount){
        Request request = new Request(amount);
        dispenser.dispense(request);
    }

    private Dispenser getChain(){

        Dispenser dispenser10 = new Dispenser(10, 100);
        Dispenser dispenser20 = new Dispenser(20, 100);
        Dispenser dispenser50 = new Dispenser(50, 100);
        Dispenser dispenser100 = new Dispenser(100, 1);

        dispenser100.setNext(dispenser50);
        dispenser50.setNext(dispenser20);
        dispenser20.setNext(dispenser10);

        return dispenser100;
    }





}
