package eu.sages.ej.gof.behavioral.strategy.payment;

public class ShoppingMain {

    public static void main(String[] args) {
        System.out.println("let's buy smth!");

        Shopping s = new Shopping();

        s.addItem(new Item("mleko", 3, 2.2));
        s.addItem(new Item("Ipa", 24, 3.5));
        s.addItem(new Item("paluszki", 2, 1.79));

        s.pay(getPaymentMethod("paypal"));

    }

    private static PaymentMethod getPaymentMethod(String method) {
        switch (method) {
            case "paypal":
                return new Paypal("user", "password");
            case "card":
                return new CreditCard("32232344", 123);
            default:
                return  null;
        }
    }
}