package eu.sages.ej.gof.behavioral.cor.purchase;

// limit: 1_000_000
public class President extends Approver {

    public President(String name, int limit) {
        super(name, limit);
    }
}
