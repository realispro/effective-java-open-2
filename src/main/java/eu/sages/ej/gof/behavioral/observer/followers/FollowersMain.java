package eu.sages.ej.gof.behavioral.observer.followers;

public class FollowersMain {

    public static void main(String[] args) {

        Profile profile1 = new Profile("profile1");
        Profile profile2 = new Profile("profile2");
        Profile profile3 = new Profile("profile3");

        profile1.follow(profile2);
        profile1.follow(profile3);
        profile2.follow(profile3);

        profile1.publish("totally unimportant message 1");
        profile1.publish("totally unimportant message 2");
        profile2.publish("totally unimportant message 3");
        profile3.publish("totally unimportant message 4");
    }
}
