package eu.sages.ej.gof.behavioral.templatemethod.encrypt;


public class EncryptMain {

    public static void main(String[] args){
        SafeStore store = new AESCipherStore();
        boolean stored = store.store("hide it!");
        System.out.println("safely stored? " + stored);
    }

}
