package eu.sages.ej.gof.behavioral.strategy.payment;

// strategy
public interface PaymentMethod {

    void charge(double value);

}
