package eu.sages.ej.gof.behavioral.cor.purchase;

public class PurchaseMain {

    public static void main(String[] args) {
        System.out.println("let's process a purchase");

        Purchase p1 = new Purchase(162_361, "samochod dla sprzedawcy");
        Purchase p2 = new Purchase(55, "papier do drukarki");
        Purchase p3 = new Purchase(5500, "laptop");

        Approver approver = ApproverFactory.getApproverChain();
        approver.approvePurchase(p1);
        approver.approvePurchase(p2);
        approver.approvePurchase(p3);

        System.out.println("p1 = " + p1);
        System.out.println("p2 = " + p2);
        System.out.println("p3 = " + p3);
    }


}
