package eu.sages.ej.gof.behavioral.mediator.chat;

/**
 * Created by xdzm on 2016-06-17.
 */
public class ChatPart {

    private Channel channel;

    private String name;

    public ChatPart(Channel channel, String name) {
        this.channel = channel;
        this.name = name;
        this.channel.join(this);
    }

    public void send(String message){
        channel.send(this, message);
    }

    public void receive(String message){
        System.out.println("[" + name + "] received: " + message);
    }

    public String getName() {
        return name;
    }
}
