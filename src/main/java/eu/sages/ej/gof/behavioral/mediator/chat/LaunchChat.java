package eu.sages.ej.gof.behavioral.mediator.chat;


public class LaunchChat {

    public static void main(String[] args) {

        Channel channel = new Channel("Effective Java Open");

        ChatPart part1 = new ChatPart(channel, "part1");
        ChatPart part2 = new ChatPart(channel, "part2");
        ChatPart part3 = new ChatPart(channel, "part3");

        part1.send("Hey man!");
        part2.send("How are U doing?");
    }
}
