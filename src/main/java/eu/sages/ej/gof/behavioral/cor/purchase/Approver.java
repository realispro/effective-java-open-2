package eu.sages.ej.gof.behavioral.cor.purchase;

public abstract class Approver {

    private String name;

    private int limit;

    private Approver next;

    public Approver(String name, int limit){
        this.name = name;
        this.limit = limit;
    }

    public void approvePurchase(Purchase p){
        System.out.println("Przyjęte do realizacji przez " + name + ": " + p);
        if(p.getAmount() > limit) {
            System.out.println("Za duża kwota");
            if(next!=null){
                next.approvePurchase(p);
            }
        } else {
            System.out.println("Zrealizowane przez: " + name);
            p.approve(name);
        }
    }
    public String getName() {
        return name;
    }
    public Approver nextApprover(Approver approver) {
        this.next = approver;
        return approver;
    }
}
