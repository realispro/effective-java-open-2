package eu.sages.ej.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TxtParser extends Parser{

    @Override
    protected String getSupportedExtension() {
        return ".txt";
    }

    @Override
    protected void doParsing(File file) throws IOException {
        Files
                .lines(file.toPath())
                .forEach(System.out::println);
    }
}
