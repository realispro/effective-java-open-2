package eu.sages.ej.gof.behavioral.templatemethod.encrypt;

public class AESCipherStore extends CipherStore{
    @Override
    protected String getAlgorithm() {
        return "AES";
    }
}
