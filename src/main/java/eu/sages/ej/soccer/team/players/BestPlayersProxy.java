package eu.sages.ej.soccer.team.players;

import java.util.List;
import java.util.stream.Collectors;

public class BestPlayersProxy implements PlayersPool{

    private PlayersPool pool;

    public BestPlayersProxy(PlayersPool pool) {
        this.pool = pool;
    }

    @Override
    public List<Player> getGoalKeepers() {
        return finByRank(pool.getGoalKeepers(), 1);
    }

    @Override
    public List<Player> getDefenders() {
        return finByRank(pool.getDefenders(), 4);
    }

    @Override
    public List<Player> getMidfields() {
        return finByRank(pool.getMidfields(), 4);
    }

    @Override
    public List<Player> getAttackers() {
        return finByRank(pool.getAttackers(), 2);
    }

    private List<Player> finByRank(List<Player> players, int count){
        return players.stream()
                .sorted((p1,p2)-> p1.getRank()-p2.getRank())
                .limit(count)
                .collect(Collectors.toList());
    }
}
