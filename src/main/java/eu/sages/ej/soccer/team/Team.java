package eu.sages.ej.soccer.team;


import eu.sages.ej.soccer.team.players.Player;

import java.util.List;
import java.util.Objects;

public class Team {

    private String name;

    private Player goalKeeper;

    private List<Player> defenders;

    private List<Player> midfields;

    private List<Player> attackers;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player getGoalKeeper() {
        return goalKeeper;
    }

    public void setGoalKeeper(Player goalKeeper) {
        this.goalKeeper = goalKeeper;
    }

    public List<Player> getDefenders() {
        return defenders;
    }

    public void setDefenders(List<Player> defenders) {
        this.defenders = defenders;
    }

    public List<Player> getMidfields() {
        return midfields;
    }

    public void setMidfields(List<Player> midfields) {
        this.midfields = midfields;
    }

    public List<Player> getAttackers() {
        return attackers;
    }

    public void setAttackers(List<Player> attackers) {
        this.attackers = attackers;
    }

    public String info(){
        return toString();
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                "\ngoalKeeper=" + goalKeeper +
                "\ndefenders=" + defenders +
                "\nmidfields=" + midfields +
                "\nattackers=" + attackers +
                "}\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return Objects.equals(name, team.name) &&
                Objects.equals(goalKeeper, team.goalKeeper) &&
                Objects.equals(defenders, team.defenders) &&
                Objects.equals(midfields, team.midfields) &&
                Objects.equals(attackers, team.attackers);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, goalKeeper, defenders, midfields, attackers);
    }

    public static class Builder{

        private Team team = new Team();

        public Builder atName(String name){
            team.setName(name);
            return this;
        }

        public Builder withGoalKeeper(Player player){
            team.setGoalKeeper(player);
            return this;
        }

        public Builder withDefenders(List<Player> players){
            team.setDefenders(players);
            return this;
        }

        public Builder withMidfields(List<Player> players){
            team.setMidfields(players);
            return this;
        }

        public Builder withAttackers(List<Player> players){
            team.setAttackers(players);
            return this;
        }

        public Team build(){
            // TODO verify consistency
            return team;
        }


    }
}
