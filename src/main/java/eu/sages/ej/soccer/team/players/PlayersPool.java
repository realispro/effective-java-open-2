package eu.sages.ej.soccer.team.players;



import java.util.List;

public interface PlayersPool {

    List<Player> getGoalKeepers();

    List<Player> getDefenders();

    List<Player> getMidfields();

    List<Player> getAttackers();


}
