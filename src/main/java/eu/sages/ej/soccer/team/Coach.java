package eu.sages.ej.soccer.team;

import eu.sages.ej.soccer.team.players.BestPlayersProxy;
import eu.sages.ej.soccer.team.players.PlayersPool;

public class Coach {

    private PlayersPool pool;

    private String name;

    public Coach(PlayersPool pool, String name) {
        this.pool = new BestPlayersProxy(pool);
        this.name = name;
    }

    public Team construct(){
        Team.Builder builder = new Team.Builder();
        return builder
                .atName(name)
                .withGoalKeeper(pool.getGoalKeepers().stream().findFirst().get())
                .withDefenders(pool.getDefenders())
                .withMidfields(pool.getMidfields())
                .withAttackers(pool.getAttackers())
                .build();
    }
}
