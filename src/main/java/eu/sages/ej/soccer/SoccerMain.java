package eu.sages.ej.soccer;


import eu.sages.ej.soccer.match.Match;
import eu.sages.ej.soccer.team.Coach;
import eu.sages.ej.soccer.team.Team;
import eu.sages.ej.soccer.team.players.pools.GermanPlayersPool;
import eu.sages.ej.soccer.team.players.pools.PolishPlayersPool;

public class SoccerMain {

    public static void main(String[] args) {

        Coach brzeczek = new Coach(PolishPlayersPool.getInstance(), "Polska");
        Coach loew = new Coach(GermanPlayersPool.getInstance(), "Germany");

        Team polska = brzeczek.construct();
        Team niemcy = loew.construct();

        Match match = new Match(polska, niemcy);

        match.play();
        match.showResult();
    }
}
