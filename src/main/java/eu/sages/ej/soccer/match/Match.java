package eu.sages.ej.soccer.match;


import eu.sages.ej.soccer.match.event.Event;
import eu.sages.ej.soccer.match.event.EventHandler;
import eu.sages.ej.soccer.match.event.EventIterator;
import eu.sages.ej.soccer.match.event.handlers.EventHandlerProvider;
import eu.sages.ej.soccer.match.event.mock.EventIteratorAdapter;
import eu.sages.ej.soccer.team.Team;

import java.util.HashMap;
import java.util.Map;

public class Match {

    private Team host;

    private Team guest;

    private Map<String, Integer> result = new HashMap<>();

    public Match(Team team1, Team team2) {
        this.host = team1;
        this.guest = team2;
        result.put(team1.getName(), 0);
        result.put(team2.getName(), 0);

        introduction();
    }

    public void introduction(){
        System.out.println("[ Presenting teams ]\n");
        System.out.println(host.getName() + " vs " + guest.getName() + "\n");
        System.out.println(host.info());
        System.out.println(guest.info());
    }


    public void score(Team t){
        Integer r = result.get(t.getName());
        result.put(t.getName(), ++r);
    }


    public void play(){
        // TODO emulate game
        EventIterator iterator = new EventIteratorAdapter(this, 10);
        while(iterator.hasNext()){
            Event event = iterator.next();
            System.out.println("event = " + event);
            EventHandlerProvider.getHandler().handleEvent(event);
        }

    }




    public void showResult() {

        int team1result = result.get(host.getName());
        int team2result = result.get(guest.getName());

        System.out.println("\n" + host.getName() + " : " + guest.getName());
        System.out.println(team1result + " : " + team2result);

        if(team1result==team2result){
            System.out.println("There was a draw");
        } else {
            Team won = team1result>team2result ? host : guest;
            Team lost = team1result>team2result ? guest : host;

            System.out.println("And the winner is: \n" + won);
            System.out.println("Sorry, boys: \n" + lost);
        }


    }

    public Team getHost() {
        return host;
    }

    public Team getGuest() {
        return guest;
    }





}
