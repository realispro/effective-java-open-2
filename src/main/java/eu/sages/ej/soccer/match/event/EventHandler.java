package eu.sages.ej.soccer.match.event;

// chain of responsibility
public abstract class EventHandler {

    private EventHandler next;

    public EventHandler(EventHandler next) {
        this.next = next;
    }

    public void handleEvent(Event event){
        if(isSupported(event.type)){
            doHandling(event);
        } else if(next!=null){
            next.handleEvent(event);
        }
    }

    protected abstract boolean isSupported(EventType type);

    protected abstract void doHandling(Event event);

}
