package eu.sages.ej.soccer.match.event.handlers;

import eu.sages.ej.soccer.match.event.Event;
import eu.sages.ej.soccer.match.event.EventHandler;
import eu.sages.ej.soccer.match.event.EventType;

public class GoalHandler extends EventHandler {

    public GoalHandler(EventHandler next) {
        super(next);
    }

    @Override
    protected boolean isSupported(EventType type) {
        return type==EventType.GOAL;
    }

    @Override
    protected void doHandling(Event event) {
        event.match.score(event.team);
    }
}
