package eu.sages.ej.soccer.match.event;

import java.util.Iterator;

public interface EventIterator extends Iterator<Event> {

}
