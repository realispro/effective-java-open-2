package eu.sages.ej.soccer.match.event.mock;

import eu.sages.ej.soccer.match.Match;
import eu.sages.ej.soccer.match.event.Event;
import eu.sages.ej.soccer.match.event.EventIterator;

public class EventIteratorAdapter implements EventIterator {

    private EventGenerator generator = new EventGenerator();

    private int count;

    private int index;

    private Match match;

    public EventIteratorAdapter(Match match, int count) {
        this.count = count;
        this.match = match;
    }

    @Override
    public boolean hasNext() {
        return index<count;
    }

    @Override
    public Event next() {
        index++;
        return generator.generateEvent(match);
    }
}
