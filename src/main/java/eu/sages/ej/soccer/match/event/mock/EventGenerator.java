package eu.sages.ej.soccer.match.event.mock;





import eu.sages.ej.soccer.match.Match;
import eu.sages.ej.soccer.match.event.Event;
import eu.sages.ej.soccer.match.event.EventType;
import eu.sages.ej.soccer.team.Team;
import eu.sages.ej.soccer.team.players.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class EventGenerator {

    public Event generateEvent(Match m){
        EventType type = randomEventType();
        Team randomTeam = randomTeam(m.getHost(),m.getGuest());
        Player p = randomPlayer(randomTeam);
        return new Event(type, m, randomTeam, p);
    }

    private EventType randomEventType(){
        return EventType.values()[
                ThreadLocalRandom.current().nextInt(EventType.values().length)];
    }

    private Team randomTeam(Team team1, Team team2){
        return ThreadLocalRandom.current().nextBoolean() ? team1 : team2;
    }

    private Player randomPlayer(Team t){
        List<Player> players = new ArrayList<>();
        players.addAll(t.getDefenders());
        players.addAll(t.getMidfields());
        players.addAll(t.getAttackers());
        players.add(t.getGoalKeeper());

        return players.get(
                ThreadLocalRandom.current().nextInt(players.size()));
    }

}
