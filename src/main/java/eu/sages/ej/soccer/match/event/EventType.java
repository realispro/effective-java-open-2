package eu.sages.ej.soccer.match.event;

public enum EventType {
    GOAL,
    YELLOW_CARD,
    RED_CARD
}