package eu.sages.ej.soccer.match.event.handlers;

import eu.sages.ej.soccer.match.event.EventHandler;

public class EventHandlerProvider {

    private static EventHandler handler =  new GoalHandler(null);

    public static EventHandler getHandler(){
        return handler;
    }
}
